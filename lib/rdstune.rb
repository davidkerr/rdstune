require 'aws-sdk'
require 'mrtuner'
require_relative 'ec2info'


module RDSTUNE
  class RdsTune < MrTuner

    def initialize(instance_type: nil, type: nil, memory: nil, options: {}, aws_credentials: {}, rds_client: nil)
      if rds_client
        @rds   = rds_client
      else
        @rds   = Aws::RDS::Client.new(aws_credentials)
      end

      if instance_type then
        @ec2info = Ec2Info.new(instance_type)
        @memory  = @ec2info.get_memory
      else
        @memory  = memory
      end
      @type    = type
      @options = options
      super(@type, @memory, @options)
    end

    def create_parameter_group(name: nil, family: nil)
      abort("Please Specify a Parameter Group Name: #{name} or Family: #{family}") unless name && family
      group_name   = name
      group_family = family
      unless find_parameter_group(group_name)
        description = "Created by RdsTune - #{@type} - #{@memory} - #{Time.now.to_s}"
        @rds.create_db_parameter_group({
          :db_parameter_group_name   => group_name,
          :db_parameter_group_family => group_family,
          :description               => description
        })
      end
      update_parameter_group(group_name)
    end

    private
    def update_parameter_group(group_name)
      begin
        @rds.modify_db_parameter_group({
            :db_parameter_group_name => group_name,
            :parameters              => generate_update(group_name)
          })
          puts "Group #{group_name } created."
          return group_name
      rescue Aws::RDS::Errors::InvalidDBParameterGroupState
        abort("Parameter group not updateable at this time " + Aws::RDS::Errors::InvalidDBParameterGroupState)
      end
    end

    def generate_update(group_name)
      parameters = get_all_parameters(group_name)
      new_config = get_config
      out        = Array.new
      new_config.each do |p, v|
        parameter  = get_parameter(p.to_s, parameters)
        low,high = parameter[:allowed_values].split('-')

        # Convert Bytes to Kibibytes for Memory parameters
        case parameter[:parameter_name]
        when 'work_mem','maintenance_work_mem'
          v = v / 1024
        when 'shared_buffers','effective_cache_size'
          v = (v / 1024) / 8
        end

        case parameter[:data_type]
        when 'float'
          v = low if v.to_f < low.to_f
          v = high if v.to_f > high.to_f
          parameter[:parameter_value] = v.to_f.to_s
        when 'integer'
          v = low if v.to_f < low.to_f
          v = high if v.to_f > high.to_f
          parameter[:parameter_value] = v.to_i.to_s
        end

        if parameter[:apply_type] == 'dynamic'
          parameter[:apply_method] = 'immediate'
        else
          parameter[:apply_method] = 'pending-reboot'
        end
        out.push(parameter)
      end
      out
    end

    def get_all_parameters(group_name)
      marker = ""
      parameters = []
      loop do
        resp       = @rds.describe_db_parameters( :db_parameter_group_name => group_name, :marker => marker)
        parameters += resp[:parameters]
        marker     = resp[:marker]
        break unless marker
      end
      parameters
    end

    def find_parameter_group(name)
      begin
        @rds.describe_db_parameter_groups( { :db_parameter_group_name => name } )
      rescue Aws::RDS::Errors::DBParameterGroupNotFound
        nil
      end
    end

    def get_parameter(name, parameters)
      parameters.select {|parameter| parameter[:parameter_name] == name}.first
    end


  end
end
