require 'net/http'
require 'json'

module RDSTUNE
  class Ec2Info
    def initialize(instance_type)
      @instance_type = instance_type
      @ec2info = JSON.parse(Net::HTTP.get(URI('https://www.ec2instances.info/rds/instances.json')))
    end

    def show()
      puts get()
    end

    def get_memory()
      m = @ec2info.select {|instances| instances['instance_type'] == @instance_type}
      m.first["memory"].to_f
    end
  end
end
