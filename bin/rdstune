#!/usr/bin/env ruby

require 'trollop'
require_relative '../lib/rdstune'
require_relative '../lib/version'

p = Trollop::Parser.new do
  version "rdstune version: #{RDSTUNE::VERSION}"
  banner <<-EOS

This gem creates a Parameter Group with sane defaults for your PostgreSQL RDS instances

Options:
EOS
  opt :type, "Type of Database Server (Web/DW/Mixed/Desktop)", :type => :string, :default => 'web'
  opt :instance_type, "Generate values based on an AWS instance type", :type => :string
  opt :memory, "Amount of Memory on the Server (GiB)", :type => :float
  opt :connections, "Specify the Target Max Connections", :type => :integer
  opt :oversubscribe, "Oversubscribe the number of connections", :type => :integer
  opt :name, "Name of your RDS Parameter Group", :type => :string
  opt :family, "Database Family (Postgres version)", :type => :string, :default => 'postgres9.4'
  opt :access_key_id, "Set the AWS Access Key for the account", :type => :string, :default => ENV['AWS_ACCESS_KEY_ID']
  opt :secret_access_key, "Set the AWS Secret Key for the account", :type => :string, :default => ENV['AWS_SECRET_ACCESS_KEY']
  opt :region, "Specify the AWS Region for the account", :type => :string, :default => ENV['AWS_REGION']
  opt :dry, "Dry Run - Do not Create", :default => false
  opt :version, "Show the current version of rdstune"
end

opts = Trollop::with_standard_exception_handling p do
  o = p.parse ARGV
  if (o[:memory] == nil or o[:instance_type] == nil) and (o[:type] == nil or o[:connections] == nil)
    raise Trollop::HelpNeeded
  end
  o
end

if opts[:instance_type]
  name = opts[:name] || "rdstune-#{opts[:instance_type]}-#{opts[:connections]}".gsub('.','-')
else
  name = opts[:name] || "rdstune-#{opts[:type]}-#{opts[:memory]}-#{opts[:connections]}".gsub('.','-')
end

options = {}
aws_credentials = { :access_key_id => opts[:access_key_id],
                    :secret_access_key => opts[:secret_access_key],
                    :region => opts[:region] }
options['connections']   = opts[:connections] if opts[:connections]
options['oversubscribe'] = opts[:oversubscribe] if opts[:oversubscribe]

rdstune = RDSTUNE::RdsTune.new(instance_type: opts[:instance_type], type: opts[:type],
                               memory: opts[:memory],aws_credentials: aws_credentials,
                               options: options)

rdstune.create_parameter_group(name: name, family: opts[:family]) unless opts[:dry]
rdstune.display_config
