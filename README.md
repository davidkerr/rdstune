# rdstune

This gem can be used to create a Parameter Group with sane defaults for your PostgreSQL RDS instances.
It can also be used in a ruby program to give programmatic access to those defaults.

Note it doesn't actually create any RDS instances so no charges should be incurred via the use of this gem.


## Requirements
  * Ruby 2.0.0 and up
  * AWS Credentials

## Installation

Configure your AWS-SDK environment. (optional)

```bash
export AWS_ACCESS_KEY_ID="AWS_ACCESS_KEY"
export AWS_SECRET_ACCESS_KEY="AWS_SECRET_KEY"
export AWS_REGION='REGION' # ex: us-west-2
```

```bash
gem install rdstune
```
If you're using rbenv you'll want to use
```bash
rbenv rehash
```
To add the shim for the binary.

## Commandline Usage

```bash
rdstune -h

This gem creates a Parameter Group with sane defaults for your PostgreSQL RDS instances

Options:
  -t, --type=<s>                 Type of Database Server (Web/DW/Mixed/Desktop) (default: web)
  -i, --instance-type=<s>        Generate values based on an AWS instance type
  -m, --memory=<f>               Amount of Memory on the Server (GiB)
  -c, --connections=<i>          Specify the Target Max Connections
  -o, --oversubscribe=<i>        Oversubscribe the number of connections
  -n, --name=<s>                 Name of your RDS Parameter Group
  -f, --family=<s>               Database Family (Postgres version) (default: postgres9.4)
  -a, --access-key-id=<s>        Set the AWS Access Key for the account (default: $AWS_ACCESS_KEY_ID)
  -s, --secret-access-key=<s>    Set the AWS Secret Key for the account (default: $AWS_SECRET_ACCESS_KEY)
  -r, --region=<s>               Specify the AWS Region for the account (default: $AWS_REGION) 
  -d, --dry                      Dry Run - Do not Create
  -v, --version                  Show the current version of rdstune
  -h, --help                     Show this message
```

* "name" is optional. If you do not specify a name one will be generated for you in the format `rdstune-$type-$memory-$connections`
* "family" is optional. Valid options are postgres9.3 and postgres9.4

### Oversubscribing

While not encouraged it's often the reality that you want to *tune* your database for a sane number of connections but *allow* more. This is especially true when working with persistent connection pools across multiple applications, where most of the time you can expect any single connection to be active, but you don't expect all connections to be used at the same time.

To accommodate this i've created the oversubscribe flag on rdstune. What this will do is tune your database for `--connections` connections but it will set max_connections to `--oversubscribe`

## Embedded Usage

```ruby
require 'rdstune'

tune = RdsTune.new (type: 'web', memory: 2, options: { connections => 200 })

myconfig = tune.get_config

puts myconfig # => {:max_connections=>200, :maintenance_work_mem=>134217728.0, :work_mem=>10485760.0, :checkpoint_completion_target=>0.7, :shared_buffers=>536870912.0, :effective_cache_size=>2147483648.0, :checkpoint_segments=>8}
```

## ToDo
  * Allow modification of the values and have that affect other areas of the config.
  * Tests
